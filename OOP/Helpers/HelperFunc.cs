﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assem1.Helpers
{
    public static class HelperFunc
    {
        public static string PromptUser(string inputName)
        {
            Console.WriteLine($"Enter a valid {inputName}");
            return Console.ReadLine().Trim();
        }
    }
}
