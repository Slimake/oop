﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assem1.Helpers;

namespace Assem1
{
    public static class AccountService
    {
        private static User _user;
        public static void Register(Register model)
        {
            var password = PasswordValidator(model.Password, model.ConfirmPassword);
            if (string.IsNullOrWhiteSpace(password)) {
                Console.WriteLine("Password does not match");
                return;
            }

            _user = new User(model.Birthday)
            {
                FullName = $"{model.LastName} {model.FirstName}",
                Birthday = new DateTime(1993, 12, 5),
                Gender = model.Gender,
                Email = model.Email,
                Password = password
            };

            Console.WriteLine($"Congratulations!, {_user.FullName}, your Registration was successful");
            Console.WriteLine($"Press 1 to login:\nPress 2 to exit");
            var input = Console.ReadLine();

            while (String.IsNullOrEmpty(input) || input != "1" && input != "2")
            {
                input = HelperFunc.PromptUser("input");
            }

            if (input == "1")
            {
                Console.WriteLine("Enter your email and password separated a with space");
                var credentials = Console.ReadLine();

                while (String.IsNullOrEmpty(credentials) || credentials.Split().Length == 1 || credentials.Split().Length > 2)
                {
                    credentials = HelperFunc.PromptUser("email and password a with space");
                }

                var email = credentials.Split()[0].Trim().ToLower();
                var passWord = credentials.Split()[1].Trim();
                Login(email, passWord);

            } else if (input == "2")
            {
                Console.WriteLine("You existed the application");
            }
        }

        public static void Login(string email, string password)
        {
            //Toggle Age privacy
            _user.ToggleAgePrivacy();
            //_user.ToggleAgePrivacy();

            if (_user.Email.ToLower() == email.ToLower() && _user.Password == password && _user.Age != null)
            {
                Console.WriteLine($"Welcome, {_user.FullName}, your Age is {_user.Age} Joined: {_user.Created}");
            } else if (_user.Email.ToLower() == email.ToLower() && _user.Password == password && _user.Age == null)
            {
                Console.WriteLine($"Welcome, {_user.FullName}, Joined: {_user.Created}");
            } else
            {
                Console.WriteLine("Invalid Login Details");
            }
        }

        private static string PasswordValidator(string password, string confirmPassword)
        {
            if (string.IsNullOrWhiteSpace(password) || string.IsNullOrWhiteSpace(confirmPassword))
                return string.Empty;
            return password.Trim() == confirmPassword.Trim() ? password : string.Empty;
        }
    }
}
