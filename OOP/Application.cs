﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using Assem1.Helpers;

namespace Assem1
{
    public static class Application
    {
        public static void Run()
        {
            var menu = new StringBuilder();
            menu.Append("Hello, Welcome To my App");
            menu.Append("\nEnter your first name: ");
            Console.Write(menu.ToString());
            var firstName = Console.ReadLine().Trim();

            //If empty or there is a space or there is a number inside user input
            while (String.IsNullOrEmpty(firstName) || (firstName.Split().Length > 1) || Regex.IsMatch(firstName, @"\d"))
            {
                firstName = HelperFunc.PromptUser("first name");
            }

            Console.Write("Enter last name: ");
            var lastName = Console.ReadLine();

            while (String.IsNullOrEmpty(lastName) || (lastName.Split().Length > 1) || Regex.IsMatch(lastName, @"\d"))
            {
                lastName = HelperFunc.PromptUser("last name");
            }

            Console.Write("Enter email address: ");
            var email = Console.ReadLine();

            while (String.IsNullOrEmpty(email) || (email.Split().Length > 1) ||
                !(Regex.IsMatch(email, @"[a-zA-Z0-9._%+-]+@[a-zA-Z]+(\.[a-zA-Z0-9]+)+")))
            {
                email = HelperFunc.PromptUser("email address");
            }

            Console.Write("Enter birthday: ");
            var birthday = Console.ReadLine();

            while (String.IsNullOrEmpty(birthday) || (birthday.Split().Length > 1) ||
                !DateTime.TryParse(birthday, out _))
            {
                birthday = HelperFunc.PromptUser("birthday");
            }

            Console.WriteLine("Choose your Gender:\n1. Male\n2. Female\n3. Prefer Not To Say");
            var gender = Console.ReadLine();

            while (string.IsNullOrEmpty(gender) || gender != "1" && gender != "2" && gender != "3") {
                Console.WriteLine("Invalid Gender Selection");
                Console.WriteLine("Choose your Gender:\n1. Male\n2. Female\n3. Prefer Not To Say");
                gender = Console.ReadLine();
            }

            var selectedGender = GenderSelection(gender);

            Console.WriteLine("Enter Password");
            var password = Console.ReadLine();

            while (String.IsNullOrEmpty(password) || password.Split().Length > 1)
            {
                password = HelperFunc.PromptUser("password");
            }

            Console.WriteLine("Confirm Password");
            var confirmPassword = Console.ReadLine();

            while (String.IsNullOrEmpty(confirmPassword) || confirmPassword.Split().Length > 1)
            {
                password = HelperFunc.PromptUser("password");
            }

            var formData = new Register
            {
                FirstName = firstName,
                LastName = lastName,
                Birthday = DateTime.Parse(birthday),
                Email = email,
                Password = password,
                ConfirmPassword = confirmPassword,
                Gender = selectedGender
            };

            AccountService.Register(formData);
        }

        private static Gender GenderSelection(string gender)
        {
            switch (gender)
            {
                case "1":
                    return Gender.Male;
                case "2":
                    return Gender.Female;
                case "3":
                    return Gender.PreferNoToSay;
                default:
                    return Gender.SelectGender;
            }
        }
    }
}
